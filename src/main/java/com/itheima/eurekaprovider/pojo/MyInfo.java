package com.itheima.eurekaprovider.pojo;

import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.HashMap;

@Component
public class MyInfo implements InfoContributor {
    @Override
    public void contribute(Info.Builder builder) {
        Map<String, String> info = new HashMap<>();
        info.put("name", "小旋风");//作者名字
        info.put("email", "wayxingwork@qq.com");
        builder.withDetail("author", info);
    }
}